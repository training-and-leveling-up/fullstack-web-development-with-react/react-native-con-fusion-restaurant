import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator, DrawerContentScrollView, DrawerItemList} from '@react-navigation/drawer';
import Menu from './MenuComponent';
import Home from './HomeComponent';
import Dishdetail from './DishdetailComponent';
import Contact from './ContactComponent';
import About from "./AboutComponent";
import {Image, StyleSheet, Text, View} from "react-native";
import {Icon} from 'react-native-elements';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import {connect} from 'react-redux';
import {fetchComments, fetchDishes, fetchLeaders, fetchPromos} from "../redux/ActionCreators";

const mapStateToProps = state => {
    return {}
}

const mapDispatchToProps = dispatch => ({
    fetchDishes: () => dispatch(fetchDishes()),
    fetchComments: () => dispatch(fetchComments()),
    fetchPromos: () => dispatch(fetchPromos()),
    fetchLeaders: () => dispatch(fetchLeaders())
})

const MenuNavigator = createStackNavigator();

function MenuNavigatorScreen({navigation}) {
    return (
        <MenuNavigator.Navigator
            initialRouteName='Menu'
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
        >
            <MenuNavigator.Screen
                name="Menu"
                component={Menu}
                options={{
                    headerLeft: () => (
                        <Icon name="menu" color='white' size={24} onPress={() => navigation.toggleDrawer()}/>
                    )
                }}
            />
            <MenuNavigator.Screen
                name="Dishdetail"
                component={Dishdetail}
                options={{headerTitle: "Dish Detail"}}
            />
        </MenuNavigator.Navigator>
    );
}

const HomeNavigator = createStackNavigator();

function HomeNavigatorScreen({navigation}) {
    return (
        <HomeNavigator.Navigator
            initialRouteName='Home'
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
        >
            <HomeNavigator.Screen
                name="Home"
                component={Home}
                options={{
                    headerLeft: () => (
                        <Icon name="menu" color='white' size={24} onPress={() => navigation.toggleDrawer()}/>
                    )
                }}
            />
        </HomeNavigator.Navigator>
    );
}

const ContactNavigator = createStackNavigator();

function ContactNavigatorScreen({navigation}) {
    return (
        <ContactNavigator.Navigator
            initialRouteName='Contact'
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
        >
            <ContactNavigator.Screen
                name={"Contact"} component={Contact}
                options={{
                    headerLeft: () => (
                        <Icon name="menu" color='white' size={24} onPress={() => navigation.toggleDrawer()}/>
                    )
                }}
            />
        </ContactNavigator.Navigator>
    );
}

const AboutNavigator = createStackNavigator();

function AboutNavigatorScreen({navigation}) {
    return (
        <AboutNavigator.Navigator
            initialRouteName='About'
            screenOptions={{
                headerStyle: {
                    backgroundColor: "#512DA8"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    color: "#fff"
                }
            }}
        >
            <AboutNavigator.Screen
                name={"About"} component={About}
                options={{
                    headerLeft: () => (
                        <Icon name="menu" color='white' size={24} onPress={() => navigation.toggleDrawer()}/>
                    )
                }}
            />
        </AboutNavigator.Navigator>
    );
}

const Drawer = createDrawerNavigator();


function MainNavigator() {
    return (
        <Drawer.Navigator
            initialRouteName="Home"
            drawerContent={props => <CustomDrawerContentComponent {...props} />}
            drawerStyle={{
                backgroundColor: '#C9DFE7',
                width: 240,
            }}
        >
            <Drawer.Screen
                name="Home" component={HomeNavigatorScreen}
                options={{
                    drawerIcon: ({focused}) => (
                        <Icon name="home" type='font-awesome' size={24} color={focused ? '#7cc' : '#ccc'}/>
                    )
                }}/>
            <Drawer.Screen
                name="About Us" component={AboutNavigatorScreen}
                options={{
                    drawerIcon: ({focused}) => (
                        <Icon name="info-circle" type='font-awesome' size={24} color={focused ? '#7cc' : '#ccc'}/>
                    )
                }}/>
            <Drawer.Screen
                name="Menu" component={MenuNavigatorScreen}
                options={{
                    drawerIcon: ({focused}) => (
                        <MaterialCommunityIcons name='menu' color={focused ? '#7cc' : '#ccc'} size={24}/>
                    )
                }}/>
            <Drawer.Screen
                name="Contact Us" component={ContactNavigatorScreen}
                options={{
                    drawerIcon: ({focused}) => (
                        <Icon name="address-card" type='font-awesome' size={22} color={focused ? '#7cc' : '#ccc'}/>
                    )
                }}/>
        </Drawer.Navigator>
    );
}

function CustomDrawerContentComponent(props) {
    return (
        <DrawerContentScrollView {...props}>
            <View style={styles.drawerHeader}>
                <View style={{flex: 1}}>
                    <Image source={require('./images/logo.png')}
                           style={styles.drawerImage}/>
                </View>
                <View style={{flex: 2}}>
                    <Text style={styles.drawerHeaderText}>Ristorante Con Fusion</Text>
                </View>
            </View>
            <DrawerItemList {...props}/>
        </DrawerContentScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    drawerHeader: {
        backgroundColor: '#512DA8',
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row'
    },
    drawerHeaderText: {
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold'
    },
    drawerImage: {
        margin: 10,
        width: 80,
        height: 60
    }
});

class Main extends Component {

    componentDidMount() {
        this.props.fetchDishes();
        this.props.fetchComments();
        this.props.fetchPromos();
        this.props.fetchLeaders();
    }

    render() {
        return (
            <NavigationContainer>
                <MainNavigator/>
            </NavigationContainer>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Main);
