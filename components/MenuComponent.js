import React, {Component} from 'react';
import {FlatList, Text, View} from "react-native";
import {Avatar, ListItem} from 'react-native-elements';
import {baseUrl} from "../shared/baseUrl";
import {connect} from "react-redux";
import {Loading} from "./LoadingComponent";

const mapStateToProps = state => {
    return {
        dishes: state.dishes
    }
}

class Menu extends Component {

    static navigationOptions = {
        title: 'Menu'
    }

    render() {
        const {navigate} = this.props.navigation;

        const renderMenuItem = ({item, index}) => {
            return (
                <ListItem
                    key={index}
                    bottomDivider
                    onPress={() => navigate('Dishdetail', {dishId: item.id})}
                >
                    <Avatar rounded source={{uri: baseUrl + item.image}}/>
                    <ListItem.Content>
                        <ListItem.Title>{item.name}</ListItem.Title>
                        <ListItem.Subtitle>{item.description}</ListItem.Subtitle>
                    </ListItem.Content>
                </ListItem>
            );
        };

        if (this.props.dishes.isLoading) {
            return (
                <Loading/>
            );
        } else if (this.props.dishes.errMess) {
            return (
                <View>
                    <Text>{this.props.dishes.errMess}</Text>
                </View>
            );
        } else {
            return (
                <View>
                    <FlatList
                        data={this.props.dishes.dishes}
                        renderItem={renderMenuItem}
                        keyExtractor={item => item.id.toString()}
                    />
                </View>
            );
        }
    }
}

export default connect(mapStateToProps)(Menu);
